module.exports = {
  root: true,
  env: {
    node: true
  },
  globals: {
    defineProps: "readonly",
    defineEmits: "readonly",
  },
  extends: [
    'plugin:vue/vue3-recommended',
    'eslint:recommended',
    '@vue/typescript/recommended',
    "plugin:cypress/recommended"
  ],
  parserOptions: {
    ecmaVersion: 2021
  },
  rules: {
    "comma-spacing": ["error", { "before": false, "after": true }],
    "curly": ["error", "all"],
    "eol-last": ["error", "always"],
    "eqeqeq": ["error", "always"],
    "indent": ["error", 2],
    "no-console": ["error", { allow: ["error"] }],
    "no-var": "error",
    "object-curly-spacing": ["error", "always"],
    "prefer-arrow-callback": "error",
    "prefer-const": "error",
    "prefer-template": "error",
    "semi": ["error", "always"],
    "sort-imports": ["error"],
    "space-before-function-paren": ["error", "never"],
    "vue/html-closing-bracket-newline": ["error", {
      "singleline": "never",
      "multiline": "never"
    }],
    "vue/max-attributes-per-line": ["error", {
      "singleline": {
        "max": 3,
        "allowFirstLine": true
      },
      "multiline": {
        "max": 3,
        "allowFirstLine": true
      }
    }],
  }
};
