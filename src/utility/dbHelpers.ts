/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { arrayUnion, doc, getDoc, updateDoc } from "firebase/firestore";
import { AgeGroup } from "../interfaces/AgeGroup";
import { Player } from "../interfaces/Player";
import { Team } from "../interfaces/Team";
import { db } from "../main";

export async function fetchPlayers(sortBy: string):Promise<Array<Player>> {
  let list: Array<Player> = [];
  const docRef = doc(db, "data", "players");
  const docSnap = await getDoc(docRef);
  if (docSnap.exists()) {
    if (docSnap.data().players) {
      list = docSnap.data().players;
      if (sortBy === 'Recently Added') {
        list = sortPlayersByMostRecent(list);
      } else if (sortBy === 'Oldest to Youngest') {
        list = sortPlayersOldestToYoungest(list);
      } else {
        list = sortPlayersYoungestToOldest(list);
      }
      return Promise.resolve(list);
    } else {
      return Promise.resolve([]);
    }
  } else {
    return Promise.resolve([]);
  }
}

function sortPlayersByMostRecent(players: Array<Player>):Array<Player> {
  return players.sort((a, b) => {
    if(a.dateAdded > b.dateAdded) {
      return -1;
    }
    else {
      return 1;
    }
  });
}

function sortPlayersOldestToYoungest(players: Array<Player>):Array<Player> {
  return players.sort((a, b) => {
    if(a.dateOfBirth > b.dateOfBirth) {
      return -1;
    }
    else {
      return 1;
    }
  });
}

function sortPlayersYoungestToOldest(players: Array<Player>):Array<Player> {
  return players.sort((a, b) => {
    if(a.dateOfBirth < b.dateOfBirth) {
      return -1;
    }
    else {
      return 1;
    }
  });
}

export async function addPlayer(p: Player) {
  const playersRef = doc(db, "data", "players");
  await updateDoc(playersRef, {
    players: arrayUnion(p)
  })
    .then(() => {
      return Promise.resolve(true);
    })
    .catch(() => {
      return Promise.resolve(false);
    });
}

export async function updatePlayers(players: Array<Player>) {
  const playersRef = doc(db, "data", "players");
  await updateDoc(playersRef, {
    players: players
  })
    .then(() => {
      return Promise.resolve(true);
    })
    .catch(() => {
      return Promise.reject(false);
    });
}

export async function fetchTeams():Promise<Array<Team>> {
  const docRef = doc(db, "data", "teams");
  const docSnap = await getDoc(docRef);
  if (docSnap.exists()) {
    if (docSnap.data().teams) {
      return Promise.resolve(docSnap.data().teams);
    }
    else {
      return Promise.resolve([]);
    }
  } else {
    return Promise.resolve([]);
  }
}

export async function addTeam(team: Team) {
  const teamsRef = doc(db, "data", "teams");
  await updateDoc(teamsRef, {
    teams: arrayUnion(team)
  })
    .then(() => {
      return Promise.resolve(true);
    })
    .catch(() => {
      return Promise.reject(false);
    });
}

export async function updateTeams(teams: Array<Team>) {
  const teamsRef = doc(db, "data", "teams");
  await updateDoc(teamsRef, {
    teams: teams
  })
    .then(() => {
      return Promise.resolve(true);
    })
    .catch(() => {
      return Promise.reject(false);
    });
}

export async function fetchAgeGroups():Promise<Array<AgeGroup>> {
  let list: Array<AgeGroup> = [];
  const docRef = doc(db, "data", "ageGroups");
  const docSnap = await getDoc(docRef);
  if (docSnap.exists()) {
    if (docSnap.data().ageGroups) {
      list = docSnap.data().ageGroups;
      return Promise.resolve(list);
    } else {
      return Promise.resolve([]);
    }
  } else {
    return Promise.resolve([]);
  }
}

export async function addAgeGroup(g: AgeGroup) {
  const groupsRef = doc(db, "data", "ageGroups");
  await updateDoc(groupsRef, {
    ageGroups: arrayUnion(g)
  })
    .then(() => {
      return Promise.resolve(true);
    })
    .catch(() => {
      return Promise.resolve(false);
    });
}
