import { AppState, AppStore } from "../interfaces/AppState";
import { User } from "firebase/auth";
import { reactive } from '@vue/runtime-dom';

const appState: AppState = {
  user: undefined
};

const appStore: AppStore = {
  state: reactive(appState),
  updateUser(u: User | undefined):void {
    this.state.user = u;
  }
};

export { appStore };
