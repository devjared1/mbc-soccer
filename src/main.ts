import '@quasar/extras/material-icons/material-icons.css';
import 'quasar/src/css/index.sass';
import { Dialog, Notify, Quasar } from 'quasar';
import App from './App.vue';
import { appStore } from './state/app';
import { createApp } from 'vue';
import { getAuth } from 'firebase/auth';
import { getFirestore } from "firebase/firestore";
import { initializeApp } from 'firebase/app';
import router from './routes';

const firebaseConfig = {
  apiKey: "AIzaSyCyduPsMPpxDOPsRRMrJMIHJ22cvhLttNk",
  authDomain: "mbc-soccer.firebaseapp.com",
  projectId: "mbc-soccer",
  storageBucket: "mbc-soccer.appspot.com",
  messagingSenderId: "810661049270",
  appId: "1:810661049270:web:2d42bc28e068b9fb73c717"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);

export const db = getFirestore(firebaseApp);

const app = createApp(App);

getAuth().onAuthStateChanged((user) => {
  if (user) {
    appStore.updateUser(user);
  } else {
    appStore.updateUser(undefined);
  }
});

app.use(Quasar, {
  plugins: { Dialog, Notify }
});
app.use(router);
app.mount('#app');
