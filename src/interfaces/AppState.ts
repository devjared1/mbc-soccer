import { User } from 'firebase/auth';

export interface AppState {
  user: User | undefined
}

export interface AppStore {
  state: AppState
  updateUser(u: User | undefined):void
}
