export interface Evaluation {
  dashTime: string
  coneDribbling: string
  longShot10: string
  longShot20: string
  longShot30: string
  goalsRight: string
  goalsCenter: string
  goalsLeft: string
  overallScore: number
}
export interface ContactInfo {
  name: string
  phoneNumber: string
}
export type Role = 'Coach' | 'Referee' | 'Team Parent' | 'None';
export interface Player {
  id: string
  addedBy: string
  dateAdded: Date
  hasPaid: boolean
  evaluationComplete: boolean
  evaluation?: Evaluation
  hasTeam: boolean
  name: string
  gender: string
  dateOfBirth: string
  playedBefore: string
  yearsPlayed?: number
  grade: string
  nightsUnavailable: Array<string>
  streetAddress: string
  city: string
  state: string
  zipCode: string
  maleGuardian: string
  femaleGuardian: string
  parentRole: Role
  contacts: Array<ContactInfo>
  medicalConditions: string
  churchAttended: string
  moreInfoAboutMBC: boolean
  shirtSize: string
  shortSize: string
}

