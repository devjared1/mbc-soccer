export interface Team {
  id: string
  addedBy: string
  dateAdded: Date
  teamName: string
  groupId?: string
  coach: string
  avgEval?: number
  players: Array<string>
}
