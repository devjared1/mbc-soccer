import { RouteRecordRaw, createRouter, createWebHistory } from 'vue-router';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import Evaluations from '../src/views/Evaluations.vue';
import League from '../src/views/League.vue';
import NotFound from '../src/views/NotFound.vue';
import Players from '../src/views/Players.vue';
import Registration from '../src/views/Registration.vue';
import SignIn from '../src/views/SignIn.vue';

/** @type {import('vue-router').RouterOptions['routes']} */
const routes: RouteRecordRaw[] = [
  {
    name: 'home',
    path: '/',
    component: Registration,
    meta: {
      title: 'Home',
      authRequired: false
    },
  },
  {
    name: 'signin',
    path: '/signin',
    component: SignIn,
  },
  {
    name: 'evaluations',
    path: '/evaluations',
    component: Evaluations,
    meta: {
      title: 'Evaluations',
      authRequired: true
    },
  },
  {
    name: 'league',
    path: '/league',
    component: League,
    meta: {
      title: 'League',
      authRequired: true
    },
  },
  {
    name: 'players',
    path: '/players',
    component: Players,
    meta: {
      title: 'Players',
      authRequired: true
    },
  },
  {
    name: 'notfound',
    path: '/:path(.*)',
    component: NotFound
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

const getCurrentUser = () =>
  new Promise((resolve, reject) => {
    const removeListener = onAuthStateChanged(
      getAuth(),
      (user) => {
        removeListener();
        resolve(user);
      },
      reject
    );
  });


router.beforeEach(async(to, from, next) => {
  if (to.matched.some(record => record.meta.authRequired)) {
    const user = await getCurrentUser();
    if (user) {
      next();
    } else {
      next({
        path: '/signin',
      });
    }
  } else {
    next();
  }
});

export default router;
